<div align="center">
<img src="header.png" alt="More Paint Bucket " width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=1772486845<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game. <br><br>

![Steam Views](https://img.shields.io/steam/views/1772486845?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/1772486845?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/1772486845?color=%231b2838&logo=steam&style=for-the-badge)
</div>

More Paint Bucket is a mod for [Besiege](https://store.steampowered.com/app/346010) which doubles the visible icons per page in the paint bucket UI and also adds a search bar and sort option. It also allows selecting all blocks with the same skin.

Useful for people with lots of skins.

## Features
- Adds a search bar to the paint bucket tool
- ..and adds sorting options
- ...and doubles the icons per page
- Adds selecting all blocks with a specific skin

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later). 
    - A bug means that VS versions *after* 2019 can only be used if VS2019 or earlier is installed with the `.NET Framework 3.5 development tools` component.
4. Press F6 to compile the mod.

## Usage
Open the paint bucket tool.
- The search bar replaces the name text at the top of the dropdown. It works as you'd expect.
- Clicking the ⌚ button changes the sort order (and the icon). ⌚ is default, `Aa` is alphabetical, and `ID` is by workshop ID.

Press `Ctrl+S` while selecting **one** block with a specific skin to select all other blocks with that skin.

## Status
This mod is feature-complete. <br>
However, additional features and fixes may be added in the future as necessary.
