﻿using Besiege.UI;
using Besiege.UI.Serialization;
using Modding;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace MorePaintBucket {
    internal class UIHandler : MonoBehaviour {

        void Awake() {
            Make.RegisterSerialisationProvider(Mod.MOD_NAME, new SerializationProvider {
                CreateText = c => Modding.ModIO.CreateText(c),
                GetFiles = c => Modding.ModIO.GetFiles(c),
                ReadAllText = c => Modding.ModIO.ReadAllText(c),
                AllResourcesLoaded = () => ModResource.AllResourcesLoaded,
                OnAllResourcesLoaded = x => ModResource.OnAllResourcesLoaded += x,
            });

            Events.OnActiveSceneChanged += OnSceneChanged;
            if (StatMaster.isMP) {
                OnSceneChanged(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
            }

            Make.RegisterComponent<UIHoverAreaUI>();
        }

        private void OnSceneChanged(Scene arg0, Scene arg1) {
            if (Mod.SceneNotPlayable()) {
                return;
            }

            Make.OnReady(Mod.MOD_NAME, () => {
                Transform container = GameObject.Find("HUD").transform.FindChild("TopBar/Mover/Align (Middle)/Buttons/5th slot/Paint Tool/BG/Advanced/Container");
                container.FindChild("r_name_text").gameObject.SetActive(false);
                Project controls = Make.LoadProject(Mod.MOD_NAME, "ExtraControls", container);

                SkinPaintTool original = container.GetComponent<SkinPaintTool>();
                List<UIHoverArea> hoverAreas = original.mouseOverArea.ToList();
                hoverAreas.Add(controls["SortButton"].GetComponent<UIHoverAreaUI>());
                original.mouseOverArea = hoverAreas.ToArray();

                SkinPaintToolOverhaul self = container.GetComponentInParent<SkinPaintToolOverhaul>();
                controls["SearchField"].GetComponent<InputField>().onValueChanged.AddListener(self.RunSearch);
                controls["SortButton"].GetComponent<Button>().onClick.AddListener(() => self.Sort(true));
                self.sortIcon = controls["SortIcon"].gameObject;
                self.sortText = controls["SortText"].GetComponent<Text>();
            });
        }
    }
}
