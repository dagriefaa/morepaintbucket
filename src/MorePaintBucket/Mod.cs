using Localisation;
using Modding;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MorePaintBucket {
    public class Mod : ModEntryPoint {

        public const string MOD_NAME = "MorePaintBucket";

        public override void OnLoad() {

            ModControllerObject = GameObject.Find("ModControllerObject");
            if (!ModControllerObject) { UnityEngine.Object.DontDestroyOnLoad(ModControllerObject = new GameObject("ModControllerObject")); }

            Events.OnActiveSceneChanged += OnSceneChanged;
            if (StatMaster.isMP) {
                OnSceneChanged(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
            }

            // uifactory
            if (Mods.IsModLoaded(new Guid("61d89dcf-88a2-4a16-8eb2-08aeed441f1d"))) {
                ModControllerObject.AddComponent<UIHandler>();
            }
            else if (!SceneNotPlayable()) {
                GameObject funnyText = GameObject.Find("HUD").transform.FindChild("TopBar/Mover/Align (Middle)/Buttons/5th slot/Paint Tool/BG/Advanced/Container/r_name_text").gameObject;
                var textObj = UnityEngine.Object.Instantiate(funnyText, funnyText.transform.parent, false) as GameObject;

                var text = textObj.GetComponent<DynamicText>();
                text.SetText("INSTALL\nUI FACTORY");
                text.alignment = TextAlignment.Center;
                text.size = 0.13f;
                text.transform.localPosition += new Vector3(0, -0.092f, 0);

                UnityEngine.Object.DestroyImmediate(textObj.GetComponent<LocalisationChild>());
                funnyText.SetActive(false);
            }
        }

        public static bool SceneNotPlayable() {
            return !AdvancedBlockEditor.Instance;
        }

        public static GameObject ModControllerObject;

        private Vector3 IconPosition(int i) {
            return new Vector3((
                i / SkinPaintToolOverhaul.NUM_ROWS) * 0.7f + 0.725f,
                (i % SkinPaintToolOverhaul.NUM_ROWS) * -0.7f,
                -1
            );
        }

        private void OnSceneChanged(Scene arg0, Scene arg1) {
            if (Mod.SceneNotPlayable()) {
                return;
            }

            Transform bucketRoot = GameObject.Find("HUD").transform.FindChild("TopBar/Mover/Align (Middle)/Buttons/5th slot/Paint Tool");

            Transform container = bucketRoot.FindChild("BG/Advanced/Container");
            SkinPaintTool original = container.GetComponent<SkinPaintTool>();
            SkinPaintToolOverhaul self = bucketRoot.gameObject.AddComponent<SkinPaintToolOverhaul>();
            self.original = original;

            original.icons.Clear();

            // adjust page buttons
            container.FindChild("Custom/Prev").localPosition += new Vector3(-0.25f, -0.02f);
            container.FindChild("Custom/r_pages_text").localPosition += new Vector3(-0.15f, -0.02f);
            container.FindChild("Custom/Next").localPosition += new Vector3(-0.1f, -0.02f);

            // hover areas (so the window doesn't close when you click on these things)
            List<UIHoverArea> hoverAreas = original.mouseOverArea.ToList();
            GameObject searchTextBack = original.transform.FindChild("BG (1)").gameObject;
            searchTextBack.AddComponent<BoxCollider>().isTrigger = true;
            hoverAreas.Add(searchTextBack.AddComponent<UIHoverArea>());
            original.mouseOverArea = hoverAreas.ToArray();

            // create new page text (and hide old one to prevent errors)
            self.pageNumText = original.pageNumText;
            GameObject copy = UnityEngine.Object.Instantiate(self.pageNumText.gameObject, self.transform) as GameObject;
            original.pageNumText = copy.GetComponent<DynamicText>();
            copy.SetActive(false);

            // set up skin buttons
            Transform iconParent = container.FindChild("Custom/Icons");
            int j = 0;
            foreach (Transform t in iconParent) {
                t.localPosition = IconPosition(j++);
                self.icons.Add(t.GetComponent<SelectSkinPaintButton>());
            }
            for (int i = j; i < SkinPaintToolOverhaul.NUM_ICONS; i++) {
                GameObject icon = UnityEngine.Object.Instantiate(iconParent.GetChild(i - j).gameObject, iconParent, false) as GameObject;
                icon.transform.localPosition = IconPosition(i);

                switch (icon.name[0]) {
                    case 'A':
                        icon.name = $"D{icon.name[1]}";
                        break;
                    case 'B':
                        icon.name = $"E{icon.name[1]}";
                        break;
                    case 'C':
                        icon.name = $"F{icon.name[1]}";
                        break;
                }

                // fix instantiated tooltips not appearing
                // for reference: the tooltip hadn't started up yet, so none of its color variables were set
                MeshRenderer[] tooltipRenderers = icon.GetComponentInChildren<Tooltip>().tooltipParent.GetComponentsInChildren<MeshRenderer>(true);
                foreach (var r in tooltipRenderers) {
                    Color c = r.material.GetColor("_TintColor");
                    c.a = 1;
                    r.material.SetColor("_TintColor", c);
                }

                self.icons.Add(icon.GetComponent<SelectSkinPaintButton>());
            }

            // resize background
            Transform background = container.FindChild("Custom/BGs");
            background.localPosition = new Vector3(0.375f, -2.6f);
            background.localScale = new Vector3(3.005f, 2.815f, 1);
        }
    }
}
