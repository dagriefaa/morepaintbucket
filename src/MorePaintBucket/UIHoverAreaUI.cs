﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MorePaintBucket {
    internal class UIHoverAreaUI : UIHoverArea, IPointerEnterHandler, IPointerExitHandler {
        public void OnPointerEnter(PointerEventData eventData) {
            isMouseOver = true;
        }

        public void OnPointerExit(PointerEventData eventData) {
            isMouseOver = false;
        }
    }
}
