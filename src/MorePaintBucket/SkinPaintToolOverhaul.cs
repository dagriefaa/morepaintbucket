﻿using Modding;
using Modding.Blocks;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace MorePaintBucket {
    public class SkinPaintToolOverhaul : MonoBehaviour {

        static readonly BlockType[] SKINLESS_BLOCKS = new BlockType[] { BlockType.BuildEdge, BlockType.BuildNode, BlockType.CameraBlock, BlockType.Pin };
        public static readonly int NUM_ICONS = 24;
        public static readonly int NUM_ROWS = 8;

        public SkinPaintTool original;
        public List<SelectSkinPaintButton> icons = new List<SelectSkinPaintButton>();

        public Text sortText;
        public DynamicText pageNumText;
        public GameObject sortIcon;

        int currentPage = 0;
        int TotalPages {
            get {
                return Mathf.CeilToInt((skinPacks.Count - 1) / NUM_ICONS) + 1;
            }
        }

        int lastSkinCount = -1;
        List<BlockSkinLoader.SkinPack> skinPacks = new List<BlockSkinLoader.SkinPack>();

        enum SortType {
            None, ID, Alphabetical
        }
        SortType _currentType = SortType.None;
        SortType CurrentType {
            get {
                return _currentType;
            }
            set {
                _currentType = value;
                Modding.Configuration.GetData().Write("CurrentType", (int)value);
            }
        }
        readonly List<string> sortTypeNames = new List<string>() { "", "ID", "Aa" };

        string lastSearch = "";

        ModKey selectPaint;

        void Awake() {
            CurrentType = (!Configuration.GetData().HasKey("CurrentType")) ? SortType.None : (SortType)Configuration.GetData().ReadInt("CurrentType");
            selectPaint = ModKeys.GetKey("Select All With Skin");
        }

        void Start() {

            BlockSkinLoader.SkinModified -= original.UpdateDisplay;
            BlockSkinLoader.SkinModified += UpdateDisplay;

            original.prevPageButton.Click -= original.PrevPage;
            original.nextPageButton.Click -= original.NextPage;
            original.prevPageButton.Click += () => PrevPage(original);
            original.nextPageButton.Click += () => NextPage(original);


            GotoPage(0);
        }

        void OnDestroy() {
            BlockSkinLoader.SkinModified -= UpdateDisplay;
        }

        void Update() {

            // update skins
            if (BlockSkinLoader.SkinPacks.Count != lastSkinCount) {
                lastSkinCount = BlockSkinLoader.SkinPacks.Count;
                Sort(false);
                RunSearch(lastSearch);
            }

            if (original.collapsed && original.transform.localPosition.y == 6f) {
                original.transform.localPosition += new Vector3(0, 1000, 0);
            }

            // select all with skin of current block
            if (selectPaint.IsPressed) {
                SelectAllWithSkin();
            }
        }

        void SelectAllWithSkin() {
            if (AdvancedBlockEditor.Instance.selectionController.Selection.Count == 0) {
                return;
            }

            BlockBehaviour selectionBlock = null;
            if (AdvancedBlockEditor.Instance.selectionController.Selection.Count > 1) {
                // selecting build surface
                foreach (BlockBehaviour b in AdvancedBlockEditor.Instance.selectionController.Selection) {
                    if (SKINLESS_BLOCKS.Contains((BlockType)b.Prefab.ID)) {
                        continue;
                    }
                    if (!selectionBlock && b.Prefab.ID == (int)BlockType.BuildSurface) {
                        selectionBlock = b;
                    }
                    else {
                        return;
                    }
                }
            }
            else {
                // selecting anything else
                selectionBlock = AdvancedBlockEditor.Instance.selectionController.Selection[0] as BlockBehaviour;

                if (SKINLESS_BLOCKS.Contains((BlockType)selectionBlock.Prefab.ID)) {
                    AdvancedBlockEditor.Instance.selectionController.Selection.ForEach(Debug.Log);
                    return;
                }
            }


            List<BlockBehaviour> query = new List<BlockBehaviour>();
            BlockSkinLoader.SkinPack selectionSkin = selectionBlock.VisualController.selectedSkin.pack;

            foreach (Block b in PlayerMachine.GetLocal().BuildingBlocks) {
                try {
                    if (b.InternalObject == selectionBlock) {
                        continue;
                    }
                    BlockSkinLoader.SkinPack s = b.InternalObject.VisualController.selectedSkin.pack;
                    if (s == selectionSkin && !SKINLESS_BLOCKS.Contains((BlockType)b.InternalObject.Prefab.ID)) {
                        query.Add(b.InternalObject);
                    }
                }
                catch (NullReferenceException e) {
                    Debug.LogError(e);
                }
            }

            AdvancedBlockEditor.Instance.selectionController.Select(query, true, true);
        }

        void PrevPage(SkinPaintTool spt) {
            currentPage = (currentPage > 0) ? currentPage - 1 : TotalPages - 1;
            GotoPage(currentPage);
        }

        void NextPage(SkinPaintTool spt) {
            currentPage = (currentPage < (TotalPages - 1)) ? currentPage + 1 : 0;
            GotoPage(currentPage);
        }

        void GotoPage(int page, bool close = false) {
            if (page <= TotalPages && page >= 0) {
                currentPage = page;
                if (!close) {
                    pageNumText?.SetText($"{(currentPage) + 1}/{TotalPages}");
                }
                for (int i = 0; i < icons.Count; i++) {
                    int num = page * NUM_ICONS + i;
                    if (num < skinPacks.Count) {
                        BlockSkinLoader.SkinPack skinPack = skinPacks[num];
                        BlockSkinLoader.SkinPack.Skin skin = skinPack?.FindAvailableSkin();
                        int id = (skin == null) ? 1 : skin.prefab.ID;
                        try {
                            icons[i].Setup(id, skin, original);
                        }
                        catch (NullReferenceException e) { }
                    }
                    else {
                        try {
                            icons[i].Setup(1, null, original);
                        }
                        catch (NullReferenceException e) { }
                    }
                }
            }
        }

        void UpdateDisplay(BlockSkinLoader.SModifier m) {
            if (m == null || !(m is BlockSkinLoader.SkinPack.Skin)) {
                return;
            }

            BlockSkinLoader.SkinPack.Skin skin = m as BlockSkinLoader.SkinPack.Skin;
            if (skin == SkinPaintTool.Skin) {
                original.SetIconDisplay(skin, false);
            }

            if (skinPacks.IndexOf(skin.pack) >= currentPage * NUM_ICONS
                && skinPacks.IndexOf(skin.pack) < (currentPage + 1) * NUM_ICONS) {

                original.SetUI(false);

                if (StatMaster.advancedBuilding && OptionsMaster.skinsEnabled) {
                    GotoPage(currentPage);
                }

                return;
            }
        }

        public void RunSearch(string search) {
            skinPacks.Clear();
            if (search == "") {
                skinPacks.AddRange(BlockSkinLoader.SkinPacks);
                GotoPage(currentPage);
            }
            else {
                foreach (BlockSkinLoader.SkinPack s in BlockSkinLoader.SkinPacks) {
                    if (CultureInfo.InvariantCulture.CompareInfo.IndexOf(s.name, search, CompareOptions.IgnoreCase) >= 0) {
                        skinPacks.Add(s);
                    }
                }
            }
            Sort(false, true);
            lastSearch = search;
        }

        public void Sort(bool next, bool fromSearch = false) {
            if (next) {
                CurrentType = (CurrentType == SortType.Alphabetical) ? SortType.None : CurrentType + 1;
            }

            switch (CurrentType) {
                case SortType.None:
                    if (!fromSearch) RunSearch(lastSearch);
                    break;
                case SortType.ID:
                    skinPacks.Sort((x, y) => x.id.CompareTo(y.id));
                    break;
                case SortType.Alphabetical:
                    skinPacks.Sort((x, y) => x.name.CompareTo(y.name));
                    break;
            }
            original.SetUI(false);

            if (StatMaster.advancedBuilding && OptionsMaster.skinsEnabled) {
                GotoPage(fromSearch ? 0 : currentPage);
            }

            if (sortText) {
                sortText.text = sortTypeNames[(int)CurrentType];
            }
            sortIcon?.SetActive(CurrentType == SortType.None);
        }

    }
}